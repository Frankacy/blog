---
layout: page
title: Thanks for coming out to CocoaHeads MTL
permalink: /cocoaheadsmtl2017/
---

I love talking about NSOperations, and I hope you got a lot out of it!

I've prepared some sample code (as well as a copy of the slides) for you to check out! You can download the goodies here: [Voila!](https://s3.amazonaws.com/ioscoachfrank.com/bonuses/chmtl2017.zip)

Also, as I mentioned during the talk, I'm preparing an online course that teaches everything you need to know about NSOperations. You'll learn the patterns, practices and pitfalls of building your own operations, and how to supercharge your development!

If you want to hear more, sign up to the waiting list below and I'll make sure to let you know when it's ready!

