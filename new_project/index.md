---
layout: page
title: Tell me about your project
permalink: /new_project/
active: consulting
---

<div class="c-page__form">
<form id="projectform" method="POST">

<div class="form-group">
<label for="what">What prompted you to start this project?<span class="c-page__form-mandatory">*</span></label>
<textarea name="Why this project?" id="what" placeholder="Tell me about why you’re starting this project and how NOT doing it would be bad for your business."></textarea>
</div>

<div class="form-group">
<label for="business">What business goals do you want to achieve?</label>
<textarea name="Goals?" id="business" placeholder="Please be specific. I'll only take on projects where my fees are less than the business value of the work I provide."></textarea>
</div>

<div class="form-group">
<label for="business">Do you have a minimum budget set aside?</label>
<textarea name="Budget?" id="business" placeholder="The more you share, the more I can help you. $20k is a minimum starting budget for most of the projects I work on"></textarea>
</div>

<div class="form-group">
<label for="business">When would you like to start this project? Finish it?</label>
<textarea name="Timeline?" id="business" placeholder="Briefly explain your expectations for the timeline of this project"></textarea>
</div>

<div class="form-group">
<label for="business">Anything else I should know?</label>
<textarea name="Anything else?" id="business" placeholder="Share as many additional details as you'd like"></textarea>
</div>

<div class="form-group">
<label for="">Who are you?<span class="c-page__form-mandatory">*</span></label>

<div class="grid">
  <div class="col-1-3">
    <input name="Name" type="text" placeholder="Your name">
  </div>
  <div class="col-1-3">
    <input name="Email" type="text" placeholder="Your email">
  </div>
</div>

<div class="grid">
  <div class="col-2-3">
    <input name="URL" type="text" placeholder="Reference URL (optional)">
  </div>
</div>
</div>

<!--
<div class="form-group">
<a href="" class="c-btn">Submit Project Details</a>
</div>
-->

<div class="form-group">
<input type="submit" value="Submit Project Details" class="c-btn">
</div>

<input type="hidden" name="_subject" value="New Project Lead" />
<input type="hidden" name="_next" value="http://ioscoachfrank.com/contact.html" />
<input type="text" name="_gotcha" style="display:none" />

</form>
</div>
<script>
    var contactform =  document.getElementById('projectform');
    contactform.setAttribute('action', 'https://formspree.io/' + 'hello' + '@' + 'frankcourville' + '.' + 'com');
</script>

