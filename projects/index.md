---
layout: page
title: Projects
permalink: /projects/
active: projects
---
## CocoaHeads Montreal

I co-organize the monthly CocoaHeads meet up in my beloved city of Montreal. If you’re in town, why not drop by and say hi? You can find the next time we’re meeting on our [website](http://www.cocoaheadsmtl.com "CocoaHeads Montreal").

## Stream Perfect
Have you ever seen someone beat Super Mario 64 in 20 minutes? Stream Perfect is a window into the speedrunning community: some of the most skilled players in the world who complete games with incredible speed and finesse. Stream Perfect finds all the best live speedrunners on Twitch.tv so you can sit back and enjoy their great display of skill. Stream Perfect is currently out of the App Store for a much needed refresh! Stay tuned 🙃

