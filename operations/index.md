---
layout: page
title: Apple’s MVC has failed you, and you want to clean up your code.
---

<article class="c-page__article">
<div class="c-page__list">
<p class="c-page__list-title">This is part 1 in a 5-part series on <a href="">NSOperation & Transformers.</a></p>

<ol class="c-page__list-items">
<li><a href="">Mocking Classes You Don’t Own</a></li>
<li><a href="">A Better Way to Make Buttons in Sketch</a></li>
<li><a href="">Default, Hover, Pressed, Disabled</a></li>
<li><a href="">My UI kit no longer has 10+ button symbols</a></li>
<li><a href="">The Golden Rule of Symbol Swapping</a></li>
</ol>

</div>
</article>

Tell me, do any of these sound familiar?

- You have massive view controllers of 1000 lines or more
- You’re often rewriting code that’s only *slightly* different
- Your architecture makes changes difficult to implement
- Your asynchronous code is hard to read and impossible to maintain
- New projects are slow to start because you can’t reuse any of your old code

And worst of all, **you know you have problems in your codebase but you don’t know how to solve them!**

## Imagine if you could write code that was both highly reusable, and highly composable.

Imagine a world where…

- Your view controllers are small and focused
- You can reuse code over and over again inside your project
- Your architecture makes implementing new requirements a breeze!
- Asynchronous code is easy to write and reason about
- You start new projects with a massive head start

**Wouldn’t that be a delightful codebase to work on?**

## Well, the technology has been around for 30 years, but no one has taught it.

Ladies and gentlemen, let me introduce you to NSOperations. They’re powerful, robust, and woefully underused.

`NSOperations` are Apple’s abstraction around work that needs to be done. Using this simple abstraction, **I’ll show you how to create your own reusable NSOperations to model everything from network requests, to parsing, to showing complete flows of UI.**

I’ll teach you how to string them together one after the other, launch them in parallel, or even make complex decision trees. And all this using composition and reusability.

**You’ll also learn the best practices that help you get the most out of your operations while avoiding the common pitfalls** that many encounter.

## Learn everything you need to know to clean up your code and jumpstart your projects

I make it a point to provide you with as many resources as possible to make sure you can leverage NSOperations in your own code. To this effect, you get:

- All 8 modules, for both Objective-C and Swift, in PDF form.
- The NSOperations I use in my own projects, that save me countless hours of development on every project.
- Multiple Playground files to help you learn and test different concepts.
- A sample application to show how NSOperations are used in a real-world app.

## All this taught by an experienced iOS consultant

Who am I? My name is Frank, and I help companies make their iOS codebases a delight to work with. I’m making this info available because I keep seeing the same issues over and over again. Companies pay me thousands of dollars for this type of information, but it’s my goal to help as many iOS developers as possible.

## This all sounds great, but…

### Can this really benefit *my* project?

**I’m convinced that all projects can benefit from NSOperations.** Whether you’re building bluetooth communication or building a CRUD app, I’m convinced you can use NSOperations to clean up your code and make reusable modules.

### Will I have to rely on any 3rd party dependencies?

Absolutely not. **Everything I teach in the course is based off of Apple’s Foundation** (and a bit of UIKit). You don’t need to worry about adding yet another third party framework to your app, or having it be discontinued by its maintainer.

### Can’t I just find this stuff in Apple’s documentation?

Apple’s documentation will tell you what NSOperations are, but not what they can do. **This is the only course dedicated to teaching you everything there is to know: the best practices, the pitfalls and the common patterns** to make your app a joy to work with.

### Will this work in Objective-C? Will it work in Swift?

**This approach works wonders in both Objective-C and in Swift.** When you purchase the course, you’ll get the Objective-C version of the course, as well as the Swift version (for the latest Swift 3). 

### Will I need to completely rearchitecture my app?

Nope! **NSOperations are built with thread safety in mind.** This means you can use them right away (for example, from inside your UIViewControllers) as you slowly move code out of your controllers and into your custom operations.

## Ready to get started?

Click below to purchase the course etc etc.

