---
layout: page
title: Series
permalink: /series/
active: series
---

Below you'll find the different series of articles on overarching topics. If you want to deep dive into something, this is the place!

## <a href="{{ site.baseurl }}{% link epic-viewcontrollercomms/index.md %}">Passing Data Between UIViewControllers</a>

Passing data between view controllers is a common stumbling block in iOS development. Even after you've shipped your first app, you wonder if there was a better way to tackle this problem. In this series, we'll explore some of the common tools at your disposal that can help make view controller communication more manageable.

## <a href="{{ site.baseurl }}{% link epic-nsoperations/index.md %}">Building robust, reliable code with NSOperations.</a>

You don't belong on the main thread, so best move as much of your off of it. In this series, we explore how to create asynchronous operations to create non-trivial interactions. You'll learn how to chain them together in series and in parallel, as well as how to abstract away commonly used operations so you can keep your code clean. Finally, we dive into UIKit, and how we can integrate Apple-provided view controllers (like UIAlertController) and our own view controllers, into our operation chains.

## <a href="{{ site.baseurl }}{% link epic-collectionviewlayout/index.md %}">Building custom layouts with UICollectionViewLayout</a>

Are you tired of trying to shoehorn your custom use-cases into flow layout? In this series, we explore how to create a custom `UICollectionViewLayout` from scratch! You'll learn the different parts of `UICollectionViewLayout` and how to override its laundry list of methods to acheive the layout that your heart truly desires. You'll also learn the magic (and madness) to getting self-sizing cells to work, and we'll even dabble in cell animations!

