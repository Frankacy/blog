---
layout: page
title: Building custom layouts with UICollectionViewLayout
permalink: /collectionviewlayout/
active: series
---

**UICollectionViewLayout is an incredibly powerful class** that allows you to place elements inside a UICollectionView in almost any way your heart desires. 

**Unfortunately, it's poorly understood and woefully underused.**

Instead, we often try to shoehorn the functionality we want into `UICollectionViewFlowLayout`, creating a whole new set of issues that are difficult to debug.

Well, that ends today. Throughout this series of articles, you'll **learn how to properly subclass UICollectionViewLayout** in order to make your very own incredible layouts.

## 1. [The components of UICollectionViewLayout]({{ site.baseurl }}{% link _posts/2018-02-19-components-collection-view-layout.md %})
In order to begin subclassing `UICollectionViewLayout`, we need to understand what we're working with. In this article, we dive into the different elements that compose a layout, and get to the core of what we're trying to accomplish.

## 2. [Building a custom UICollectionViewLayout from scratch]({{ site.baseurl }}{% link _posts/2018-02-28-layout-from-scratch.md %})
In order to create your own layout, you'll need to subclass `UICollectionViewLayout`. The documentation is very good at telling you *what* to subclass, but it doesn't say much about *how*. In this article, we dive head first into `UICollectionViewLayout` and how to create our own custom layout.

## 3. Adding self-sizing cells to a UICollectionViewLayout
Getting Auto Layout to play nicely with `UICollectionViewLayout1 can be an exercise in frustration. In this article, we'll investigate the lifecycle that allows us to implement self-sizing cells and we'll identify the hoops we need to jump through so everything *just works*.

## 4. Animating insertions and deletions in UICollectionViewLayout
Finally, we'll take a stroll through the different animation APIs that can help us make our collection view more dynamic and pleasing.

I really hope this deep dive into `UICollectionViewLayout` will help you wrangle this misunderstood UIKit class. If it does, I'd love to hear about it on [Twitter](https://twitter.com/frankacy)! Let me know ☺️

