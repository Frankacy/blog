---
layout: page
title: Your download has been sent!
permalink: /thankyou-download/
---

Alright! I just sent your bonus to your email. You should be receiving it any minute 😃

### But until then...

I like to [hang out on Twitter](http://www.twitter.com/frankacy). Come say hi! I'd love to hear from you ☺️

See you soon!

-Frank
