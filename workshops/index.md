---
layout: page
title: Upcoming Online Workshops
permalink: "/workshops/"
active: workshops
---

<p>Online workshops with Frank are efficient way to increase your skills without jeopardizing your development schedule. Each half-day workshop is jam-packed with real-world examples and actionable techniques to help take your app (and its maintenance) to the next level!</p>

<div class="w-panels">
    {% for workshop in site.workshops %}
        {% assign today_date = 'now' | date: '%s' %}

        {% for date_time in workshop.next_ws_dates %}
            {% assign workshop_date = date_time.date | date: '%s' %}
            {% if today_date < workshop_date %}
                <a class="w-panel" href="{{ workshop.url }}">
                    <article>
                        <div class="w-panel__content">
                            <h2 class="w-panel__title">{{ workshop.title }}</h2>

                            <time class="w-panel__date">{{ date_time.date | date:"%b %-d, %Y" }} &#8212; {{ date_time.time }}  &#8212;  Remotely over Zoom</time>

                            <div class="w-panel__text">
                                    {{ workshop.blurb }}
                            </div>

                            <div>
                                <button class="c-btn w-panel__button">Book my ticket!</button>
                            </div>
                        </div>
                    </article>
                </a>
            {% endif %}
        {% endfor %}
    {% endfor %}
</div>

<h2>Past Workshops</h2>

<p>These workshops have already passed, but they will definitely be scheduled again in the future. Join the waitlist and get notified when they become available again! Secure your spot before anyone else and get a special discount too!</p>

<div class="w-panels">
    {% for workshop in site.workshops %}
        {% assign today_date = 'now' | date: '%s' %}
        {% assign dates = workshop.next_ws_dates | map: "date" %}
        {% assign all_before_today = true %}

        {% for date in dates %}
            {% assign check_date = date | date: '%s' %}
            {% if check_date > today_date %}
                {% assign all_before_today = false %}        
                {% break %}
            {% endif %}
        {% endfor %}

        {% if all_before_today == true %}
            {% if workshop.display == true %}
                <a class="w-panel w-panel--half" href="{{ workshop.url }}">
                    <article>
                        <div class="w-panel__content">
                            <h2 class="w-panel__title">{{ workshop.title }}</h2>

                            <div class="w-panel__text">
                                {{ workshop.blurb }}
                            </div>

                            <div>
                                <button class="c-btn w-panel__button">Join the waitlist</button>
                            </div>
                        </div>
                    </article>
                </a>
            {% endif %}
        {% endif %}
    {% endfor %}
</div>
