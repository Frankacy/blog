---
layout: page
title: Training Services
permalink: /training/
active: training
---

It’s no secret: **poorly built software is expensive to maintain.**

My name is Frank, and I’ve made it my mission to unlock the maximum potential of your iOS team. Why? Because **I’m convinced training is one of the best investments you can make as an employer:**

- You’ll benefit from confident employees that ship on time and under budget.
- Employees feel more satisfied at work and more confident at their craft.

**Unlike other software trainers, I work with your technical lead to discover the best way to maximize our time together.** Whether it’s a one-day workshop to build specific technical skills, or a broader training to build familiarity with a new topic, you know **I got you covered.**

## One day workshops

These workshops were built with a single purpose in mind: to **help your team tackle some of the most difficult challenges** mobile teams face in large organizations.

Through a mix of slides, exercises and presentations, your team will build off of the competencies they already have to build the skills they need to make effective decisions and software design.

Give your team the foundation they need in order to take on such massive endeavours. **Save yourself weeks of headaches with only a day’s-worth of training.**

Currently, I offer the following workshops:

- How to keep apps snappy with better concurrency.
- Sharing more modules between teams.
- Getting rid of Interface Builder.
- Getting started with iOS testing.
- Getting started with iOS accessibility.
- Applying SOLID principles for easier maintenance.

**Interested? Sign up below!**

