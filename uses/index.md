---
layout: page
title: The Stuff I Use
permalink: /uses/
---

## For iOS Development

### Xcode with [XVim](https://github.com/XVimProject/XVim2)

Xvim has become such an essential part of Xcode for me, I can't imagine developing without it. The combination of Vim text editing commands and Xcode to jump between files is hard to beat. There's definitely a learning curve, and sometimes it slows down on very large files, but it's leaps and bounds better than having to move the text cursor with a mouse. 

It makes me feel like a wizard. 🧙🏻‍♂️

## On the Desktop

### Omnifocus

Omnifocus is the app that single-handedly keeps my life together. I use it to track *all* my projects, whether it's for this site, client work or personal stuff. If you ever want to get me going at a conference, just ask me about GTD and Omnifocus and I'll go on and on for hours 😅

FWIW, I tried many, many, alternatives like Things 3, but they all fall short in one major way or another.

### Drafts

Drafts is a recent addition to the list because until recently I did all my writing in Ulysses. Don't get me wrong though, Ulysses is a wonderful app. However, there are two Drafts.app features that just work so well for me: Workspaces and Actions. Being able to use different workspaces to figure out the next thing to write has greatly simplified my process. It made it easier for me to get started, which has always been the hardest part for me. Drafts Actions have helped me automate parts of my process, like converting articles from Markdown to get them ready for the website.

### Alfred App

I *love* Alfred. This little launcher makes using the Mac such a delight to use. Once you have everything properly set up, you can fly from application to application and use workflows to integrate directly into the tools you use most. I find it helps keeps me in my state of flow.

I map Alfred to CMD-Space and remove the spotlight shortcut. 

### Fantastical

Fantastical is my calendar app of choice, but I can't really tell you why. I've been using it for so long, I don't quite remember where its feature set diverges from Apple's app.

### Tweetbot

Tweetbot is the app that makes Twitter useable for me. While the Twitter app is well engineered, all I *really* want is a chronological timeline of the people I follow.

### Apple Music

Apple Music has its quirks, but it's still the best integrated choice on Apple platforms, so that's what I go with. I usually listen ambient electronic (Carbon Based Lifeforms), electro-funk (Valair, Chromeo) and 2004-era Jpop ☺️

### Slack

A necessary evil, but alas, this is the world we live in now 😅 Come say hi on ios-developers.io 👋🏻

### 1Password

1Password is absolutely essential, can't live without it. Probably the second thing I install on a new system, right after Alfred.

### Keyboard Maestro

After spending many years trying to get Text Expander and Typinator to work for me, I ended up moving all that stuff to Keyboard Maestro and now I'm *finally* happy with how I got things running. I also use KM to 

## My Perfect Office

I'm a big fan of crafting a work environment that supports you in your work. 

### Ergonofis Standing Desk

Ergonofis does something that I've never seen other standing desk companies do: they integrate the standing desk controls flush with the desk itself, instead of just bolting them on underneath. They're a local company that makes an amazing product, and I'm more than happy with my desk.

That being said, I don't stand *that* often, but when I do, it makes the price worth it. I especially like it when I'm rummaging through stuff in my office, since I can put my chair underneath the desk and unlock so much room for activities. It also makes a big difference when podcasting.

### LG 4k and 5k monitors

These monitors are very *alright*. I have them mounted on an Amazon Basics dual monitor arm (which is an amazing monitor arm, btw). I use my MacBook Pro with the lid closed in a vertical stand, and sometimes it takes a few tries before the monitors detect correctly. I'm still holding out for official Apple monitors, but until that time comes, I'll use with these.

Another fun little quirk: I use the 4k in portrait orientation. I find it great for reading long articles, displaying documentation and research, or simply having Fantastical and Omnifocus open while I use my main monitor to focus on the work.

### ErgoDox EZ keyboard with Dvorak layout

I've been using mechanical keyboards for *years* now, and this one is by far my favourite. It has Cherry MX-Blue switches for maximum noise and disruption to others 🤓 I like the fact I can program multiple layers, but I try not to go too crazy with this stuff because I want to make sure I can use a normal keyboard.

That being said, I find the keyboard is needlessly bulky. I wonder if the custom-built ergodoxes have a shorter profile 🤔

As for Dvorak, well, this is an interesting story. I wanted to learn how to touch type, but I couldn't get rid of my bad habits typing in QWERTY. Instead of trying to break my bad habits, I decided to start from scratch with a new keyboard layout. It was painful few months, but now it's second nature 🤓

### Logitech MX Master 2 Mouse

As much as I dislike using a mouse, sometimes you don't have a choice. I used the Apple Magic Mouse for a long time, but after too many Bluetooth failures and dead batteries, I decided it was time for a change. I really like the feel of the MX Master, and the extra buttons actually feel useful. I use the big thumb button for exposé, and the thumb wheel to scroll between spaces.

### Herman Miller Aeron chair

This is one of the best (if not *the* best) ergonomic desk chairs money can buy. I bought it a while ago, and despite using it every day, the Aeron still looks and feels brand new, and it's hella comfortable. 

One great feature is that you can slightly tilt the whole chair forward to force yourself to use your leg muscles. This helps when you're sitting for a long period of time.

## Behind the Blog

### Jekyll

This whole blog is statically generated using Jekyll. Sometimes it requires me to jump through hoops to get things to work, but all in all, I'm very happy with this choice. It just simplifies hosting so much. My hosting bill is about 50 cents a month 🤑


