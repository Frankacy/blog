---
layout: page
title: Passing Data Between UIViewControllers
permalink: /viewcontrollercommunication/
active: series
---

There are many ways to get data from one controller to another. However, like most things in software development, some ways are better than others.

In this series, we’re going to look at the problems with the common approach of passing data (i.e. the approach we see from Apple) and then, we’ll see how we can improve things without straying too far from the “Apple way”.

		
## 1. <a href="{{ site.baseurl }}{% link _posts/2019-03-13-the-problem-with-apples-view-controllers.md %}">The Problem With Apple's View Controllers</a>

If you use storyboards the way the way our fruit overlords tell you to, then there’s no doubt about it: your view controllers are tightly coupled. In this article, we’ll explore the pitfalls of their approach as we learn about coupling and when to minimize it.

## 2. <a href="{{ site.baseurl }}{% link _posts/2019-03-27-delegates.md %}">Create Concise View Controllers by Using the Delegate Pattern</a>

Keeping view controllers small and concise is an exercise in constant vigilance. In this article, we’ll look at how we can improve our `UIViewController` design by using the delegate pattern to communicate with the rest of our app.

## 3. How to string many view controllers together using Coordinators

The coordinator pattern is the most useful pattern that Apple doesn’t teach. It helps keep your view controllers decoupled and focused. In this article, we’re going to explore how we can use this pattern to create a flow of multiple view controllers.

## 4. Avoid global state by using the responder chain

Sometimes, communicating something from one end of our app to the other is just really, really difficult. In order to do so, we use `Notification`s or a singleton to get the message across. In this article, we’ll look at a different approach built right into UIKit: the responder chain.

I hope these strategies give you a better idea about how to work with `UIViewController`, and keep them small. If they do, give me a shout on [Twitter](https://twitter.com/frankacy) and let me know ☺️


