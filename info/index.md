---
layout: page
permalink: /info/
---

# Name

These are correct:

- Frank
- Frank Courville
- François Courville (not preferred, but technically correct 😬)

# Photo

Please use the following photo:

![Decoration View Example]({{ site.url }}/assets/img/avatar.jpg)

# Bio

## Long First Person

Hi there! My name is Frank and I've been building iOS apps since the era of felt backgrounds. I'm now a full-time Swift and iOS educator, helping teams stay on the cutting edge of iOS development. I like to write about iOS on my website, talk about productivity on my podcast, and ramble about Transformers to anyone who'll listen.

## Long Third Person

Frank has been building iOS apps since the era of felt backgrounds. He now works as full-time Swift and iOS educator, helping teams stay on the cutting edge of iOS development. He likes to write about iOS on his website, talk about productivity on his podcast, and ramble about Transformers to anyone who'll listen.

## Short Firt Person

My name is Frank Courville and I'm a full-time Swift and iOS educator, helping teams stay on the cutting edge of iOS development. I love to write about iOS, podcast about productivity, and ramble about Transformers.

## Short Third Person

Frank is a full-time Swift and iOS educator, helping teams stay on the cutting edge of iOS development. He loves to write about iOS, podcast about productivity, and ramble about Transformers.

