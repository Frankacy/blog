---
layout: post
title: "2015 Year in Review"
date: 2016-04-04T21:54:27-04:00
tags:
- meta
---
I can’t think of a better way to start off the *metablog* than with a year in review. I’m eager to dive in, but first, some history.

A few years ago, I stumbled across Nathan Barry’s blog. At the time, I was working a soul-crushing full time job. You know, the type of job that makes you contemplate whether this ‘adulthood’ thing is really for you. Well, fortunately for me, I read Nathan Barry’s year in review post and was instantly hooked. For the next few months, I simply couldn’t stop thinking about freelancing, products, and multiple revenue streams. The promise of being in full control of my own destiny was too enchanting to let go. 

And now, through an incredible series of events, here I am as an independent iOS consultant, writing my very own year in review. I’m hoping this will inspire others in the same way Nathan inspired me. It **is** possible to make it on your own, to build something for yourself, and to deliberately make your life better. It’s not easy, but now I know it’s certainly possible.

On that note, lets dive into the props and slops of my first full year of freelancing!

## What went well:

### Revenue is up!
As a matter of fact, revenue is up to roughly 113k CAD, which i s far more than I was making at my last full-time job. More importantly though, the work is profoundly rewarding. I  attribute this to charging a decent hourly rate right from the get-go (thanks Brennan!), and sticking to it when negotiating. 

On the flip side, a disproportionate amount of this revenue is from a single client. While part of me feels like they would keep me onboard forever, I know this isn’t a sustainable way to build a business. I’ll be slowing down some of that work in 2016 in order to focus on growth. That means more writing, more reaching out and, if things go well, more conferences!

### CocoaHeads is growing
I help organize the local iOS and Mac developer meetup in Montreal, an endeavour I find incredibly gratifying. However,  when I inherited the meetup from the last batch of organizers, it was in dire shape. Fortunately, with the help of newly onboarded members Romain and Luc-Olivier, we’ve been able to revamp the website, create new events, and drive engagement up. 

And through all this, the one thing I'm most proud of is the fact that I *asked for help*. I had been feeling overwhelmed with the organization and simply felt I couldn’t do it alone. I wish I had built a bigger team months before. Chalk this one up to personal development!

### Public speaking skills
About two years ago, I joined a local Toastmasters club to become a better public speaker. Little did I know what I was actually getting myself into!

Toastmasters has been nothing short of transformational. From the people you meet to the confidence you gain, whatever effort you put in is returned tenfold.

I feel like I’m just now starting to find my voice and have fun in front of crowds. I’m eager to practice more, whether it be at our weekly meetings, at CocoaHeads, or during client presentations. Public speaking has, in a way, become my secret weapon for knocking peoples’ socks off. It's incredibly empowering to love something that most people dread.

### Organization and habits
Despite keeping myself busy with client work, CocoaHeads and Toastmasters, I managed to build better habits. I eat better, lost weight, started going to the gym regularly, *learned to freakin’ swim*, became a better listener, and still strive to never drop the ball.

And in the spirit of never dropping the ball, having a well-tuned GTD has proved instrumental in my success. My world would cave in without it. To those who’ve never experienced GTD: it’s a beautiful methodology that will help you keep many plates spinning. But more importantly, it teaches you to be honest with yourself, your ambitions and your fears. Those who know me often joke that when I start talking about GTD, I don't shut up. They're right.

## What could’ve gone better:
### Managing clients
I feel like I did an abysmal job in managing my clients in 2015. There was always “one more” feature to be added, “one more” thing to tweak, to get just right. Scope creep is a very real, 

However, part of me glad to have been burned first hand by the toxicity of scope creep. Looking back, it's quite clear what went wrong, and I'm not going to make those same mistakes again.

### Automation
As the months go by, I realize I waste a lot of time doing the same tasks over and over again. I’ve made a tiny bit of progress here, but I still have a long way to go. I feel there’s are a bunch of low-hanging fruit here.

## The focus for 2016
My #1 focus for 2016 is building my personal brand. Using this blog, Cocoaheads, and (hopefully) conference talks, I want to reach more people, teach them something useful, and make the world a better place for it.

I also have an aggressive revenue goal for 2016 that I want to hit. I believe it’s realistic, but the tactics that will get me there are far more elaborate than the ones that got me to this point. Deviating from what's 

And to finish up, there are a few people to whom I feel an overwhelming amount of gratitude. I want to thank the lovely Shelley Tran for always being by my side and believing in what I do. I want to thank my parents for providing me with a safety net that I never needed, but was. And finally, I want to thank @NathanBarry, who unintentionally set all this in motion. 

I can hardly believe I've made it this far. I can't wait to see where I'll end up.


