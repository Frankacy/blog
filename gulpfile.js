var gulp = require('gulp'),
	browserSync = require('browser-sync'),
	postcss = require('gulp-postcss'),
	csswring = require('csswring'),
	lost = require('lost'),
	sass = require('gulp-sass'),
	prefix = require('gulp-autoprefixer'),
	cp = require('child_process'),
	uglify = require('gulp-uglify'),
	ghPages = require('gulp-gh-pages'),
	messages = {
		 jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build'	
	};


gulp.task('jekyll-build', function (done) {
    browserSync.notify(messages.jekyllBuild);
    return cp.spawn('jekyll', ['build'], {stdio: 'inherit'})
        .on('close', done);
});

gulp.task('jekyll-rebuild', ['jekyll-build'], function () {
    browserSync.reload();
});

gulp.task('browser-sync', ['sass', 'jekyll-build'], function() {
    browserSync({
        server: {
            baseDir: '_site'
        }
    });
});	

gulp.task('sass', function() {
	var processors = [
		lost,
		csswring
	];

	return gulp.src('assets/scss/style.scss')
	.pipe(sass({
		includePaths: ['scss'],
		onError: browserSync.notify
	}))
	.pipe(prefix(['last 10 versions','ie 9'], {cascade: true}))
	.pipe(postcss(processors))
	.pipe(gulp.dest('_site/assets/css'))
	.pipe(browserSync.reload({stream: true}))
	.pipe(gulp.dest('assets/css'));
});

gulp.task('uglify', function() {
	return gulp.src('assets/js/*.js')
	.pipe(uglify())
	.pipe(browserSync.reload({stream: true}))
	.pipe(gulp.dest('_site/assets/js'));
});

gulp.task('watch', function() {

	gulp.watch('assets/scss/**/*.scss',['sass']);
	gulp.watch('assets/js/*.js',['uglify']);
	gulp.watch(['index.html','_includes/*.html','_data/*.yml','_config.yml'],['jekyll-rebuild']);
});

gulp.task('deploy', function() {

	return gulp.src('_site/**/*')
	.pipe(deploy())
});

gulp.task('default',['browser-sync','watch']);