---
layout: page
title: Are you tired of Massive View Controllers?
permalink: /smaller_view_controllers/
---

You’re not the only one. Dealing with Massive View Controllers is the #1 problem people have when learning iOS.

We’ve all been there before.

- Your controllers are way too big, making changes difficult to do.
- Bugs are obscure, take hours to reproduce, and even longer to fix.
- Most of your application state is in singletons, causing race conditions.
- You need to take on massive technical debt in order to ship on time.

And of course, **you feel dread whenever you open a view controller**.

## Now imagine how productive you could be if your view controllers were more manageable?

- You wouldn’t have to search through 3000 line controllers in order to make a small change.
- Fixing bugs would be straightforward and quick.
- You wouldn’t have to worry about making changes that could break other things in your app.
- You could ship more features, in less time, with more confidence.

And most importantly, **you could make iOS development fun again!**

## I can teach you the mindset that will help you avoid Massive View Controllers forever.

Let me show you how you can keep your view controllers and your app architecture healthy, all by following a few simple principles.

**Unlike others, I’m not simply going to show you a bunch of tricks. I’m going to teach you the mindset you need in order to never have this problem again!**

Are you ready to bring your iOS development career to the next level?

## Sign up to the only free course that will teach you to solve this problem once and for all!

<script async data-uid="d127f00c7f" src="https://f.convertkit.com/d127f00c7f/ba665056da.js"></script>
