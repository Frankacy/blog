---
layout: page
title: You're Almost Done! Do This Now...
---

Thanks for signing up! You have one thing left to do...

## Step 1: Go to your inbox and open the email from "Frank Courville"

I just sent you an email to confirm your subscription.

It should look like this:

![Look for an email from Frank Courville]({{ site.url }}/assets/pageimg/confirm_email/inbox-blurred.png)

## Step 2: Press the big "Confirm Your Subscription" button

Open the email and smash the big "Confirm Your Subscription" button.

And that's it! Thanks for signing up! If you don't the next email right away, just wait for a moment. It should arrive within a few minutes of signing up. 

## Bonus Step 3

While you're at it, [follow me on Twitter](https://www.twitter.com/frankacy){:target="_blank"}! I'd love to hear about you and your projects ☺️ My DMs are always open, so send me a message and say hi!





