---
layout: page
title: Learn the skills you need to become an experienced iOS developer 
permalink: /about/
---

Hi there! My name is Frank Courville and I’m passionate about 2 things: iOS and teaching 🤓

Years ago, I transitioned from a soul-sucking corporate job to start developing apps. And let me tell you, the transition was far from easy. 

Coming from a world filled with best-practices and rigor, iOS felt like the wild west! Things like architecture and clean code were rarely discussed. For example, Massive View Controllers were a common problem, but no one was writing about what to do about them!

While things have gotten better since, this is the type website I would’ve wanted. A website that goes beyond the basics. A website that doesn’t strive for the easy solution, but the best solution. A website that focuses on what’s practical and useful instead of what’s esoteric.

Learning the ropes while shipping projects was not fun. That’s why I’ve made it my goal to help you bridge the gap between where you are and where you want to be.  

There’s no point in having others struggle through the same process.

I hope you’ll join me on this journey of learning and discovery!

<script async data-uid="f92ad0a835" src="https://f.convertkit.com/f92ad0a835/c8bd54553b.js"></script>

<br/>

# Other stuff I’ve been working on:

- I helped run the Montreal iOS meetup: [CocoaHeads MTL](http://www.cocoaheadsmtl.com){:target="_blank"} 
- I’m building a Magic: the Gathering card database and deck building app called Net Deck. Hopefully done before 2020 😅 

## Follow Me

Here are a bunch of places where you can find me. Come say hi!

- [Twitter](https://www.twitter.com/frankacy)
- [LinkedIn](https://www.linkedin.com/in/frankcourville/)

