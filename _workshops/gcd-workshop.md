---
layout: workshopPage
title: Mastering iOS Concurrency with GCD
thumbnail_image: '../assets/img/bio.jpg'
next_ws_dates:
    - {date: 2019-11-14, time: "12:30 to 5:00 EST"}
blurb: In this workshop, we'll cover the basics of using GCD, how Apple's concurrency model works, and how to debug concurrency issues. When you're finished, you'll be able to parallelize work in your application, as well as fix or prevent concurrency issues in your app.
waitlist: { data-uid: "cddfc344d3", src: "https://coach-frank-consulting.ck.page/cddfc344d3/index.js" }
display: true
active: workshop
---

## Learn the foundational concepts for correctly using Grand Central Dispatch

Everything about concurrency is difficult.

- It's difficult to reason about, because you have no guarantee on when your code is executed.
- It's difficult to implement well, because concurrency problems can only be found at runtime.
- It's difficult to debug, because issues are erratic and sometimes impossible to reproduce.

**Most iOS developers rarely take the opportunity to dive into concurrency to truly understand the tools they have to work with.**

This is where I come in!

In this workshop, we'll cover the basics of using GCD, how Apple's concurrency model works, and how to debug concurrency issues. When you're finished, you'll be able to parallelize work in your application, as well as fix or prevent concurrency issues in your app.

## Topics

- What is concurrency? How is it different from parallelism?
- The basics of DispatchQueues and their guarantees
- Apple's recommendations
- Different GCD patterns
- GCD queue hierarchies
- Creating thread safe objects
- Producer/Consumer problems
- Using Thread Sanitizer to find data races
- Using Instruments to find common concurrency problems

## Prerequisites

- Junior to intermediate proficiency with Swift
- Xcode 11

## Additional information:

This event will occur from **1:00 PM to 5:30 PM Eastern** on **November 14** with a 30 minute break. **Tickets are non-refundable.** Tickets are transferable. Prices are in USD.

