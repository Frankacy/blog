---
layout: workshopPage
title: Dive Into UIKit Accessibility
thumbnail_image: '../assets/img/bio.jpg'
next_ws_dates:
blurb: In this workshop, we'll learn about accessibility and the breadth of APIs available to developers. We'll see how we can audit an app for accessibility issues, and of course, how to fix them. What's more, we'll look at how we can make our app more accessible without making our code more complicated than it already is.
waitlist: { data-uid: "000906e1e4", src: "https://coach-frank-consulting.ck.page/000906e1e4/index.js" }
display: true
active: workshops
---

## Is Accessibility Really Worth it? YES!

Proper accessibility is the type of thing that we all *know* we should implement, but we often don't find the time to do it. What developers don't know is that with minimal effort, you can offer a truly great accessibility experience.

## What you'll learn

In this workshop, we'll explore what accessibility means for different types of users, and we'll dive into the full breadth of what's available to us as developers.

You'll learn about the different accessibility APIs and how to account for them without spreading `if` statements everywhere throughout our code.

You'll learn different ways to audit your app and find accessibility issues, and of course, how to fix them.

## Topics

- An overview of accessibility and why it's important
- Visual affordances and how to make them easy
- Dynamic Type, and what to do when the *huge* sizes break your UI
- How to use VoiceOver and Accessibility Inspector to audit your app
- How does UIAccessibility work and how to implement its tools like
    - Basic Accessibility Properties
    - UIAccessibilityCustomAction
    - Accessibility swipes
    - UIAccessibilityContainer
    - Custom Rotors
- Implementing Guided Access

## Prerequisites

A Mac running the latest version of Xcode, and an iOS device with which to test using Voice Over.

## Additional Information

This workshop will take place remotely via Zoom. **Tickets are non-refundable.** Tickets are transferrable. Prices are in USD.
