---
layout: page
title: Subscription Confirmed!
permalink: /thankyou-5days/
---

Alright! You're now subscribed to the '5 Days to Smaller View Controllers' course.

I just want to say thanks for welcoming me into your inbox. This is not a privilege I take lightly. I'm going to do my absolute best to provide you with practical tools to grow and succeed.

### But until then...

I like to [hang out on Twitter](http://www.twitter.com/frankacy). Come say hi! I'd love to hear from you ☺️

See you soon!

-Frank
