---
layout: post
title: Getting Distractions Under Control
---

Distraction has been at the forefront of my mind recently because, more so than ever, I feel like I've finally gotten a handle on this problem. Sitting down to do focused work was becoming very difficult. And I know I'm not the only one to feel like this either.

Together, in this article, we'll unpack:

- What distractions are
- Why it's important to keep them in check
- How we can audit our own behaviour
- Some strategies to get back on track

Let's hop right in!

## We're All Really Distracted 😵

**Have you ever opened a web browser to search for some documentation, and suddenly, 15 minutes later, you find yourself scrolling through Reddit?**

Why does this happen?

But before we dive into this question, we first need to understand habits and how they work. 

Habits are the super-highway to cognitive ability. **Do you remember when you first learned how to drive?** There were so many things to keep track of! Your speed, the speed of others, lights and signs, pedestrians... **It's all very overwhelming. But I bet that as you repeated the behaviour, it became more natural.** Maybe even automatic.

Yup, that's a feature. Whenever we go through some sort of routine, we slowly cement that routine in our minds. **This is how habits are formed.** And for good reason! Can you imagine being constantly overwhelmed every time you had to drive somewhere? That'd be exhausting!

As you can probably guess, **distractions are also a kind of habit.** They trigger on a negative emotion (like boredom) and send us into a routine. Our lizard brain does this because it wants to spare us the negative emotions of boredom, frustration, etc.

But what reinforces these distractions are the reward; the reward of avoiding "pain" and replacing it with tweets, or cat pictures, or my favorite, [corgis!](https://www.ranker.com/list/cutest-corgi-pictures/pet-project). This cycle of Trigger-Routine-Reward (commonly called the habit loop) is what has been programming all our habits since the day we were born.

**When we were hunter gatherers, this did a great job at keeping us alive. But for writing software, it's problematic.**

Here's something that often happens to me: I start running my tests, and suddenly I find myself catching up on Twitter. And the kicker is when, 10 minutes later, I realize my tests haven't actually run because they couldn't even build 🤦‍♂️

It'd be easy for me to say that the trigger is running tests, but it's actually a bit deeper than that. The trigger for me is *boredom*. Waiting for my thousands of tests to run is boring, especially when I have UI Tests that can take upwards of 30s to fully complete.

And what happens when boredom triggers? In under a second, this series of events unfold:

1. I summon Alfred
2. I type in R-E-D
3. Alfred autocompletes this to Reddit.com
4. I hit enter
5. My brain, satisfied that I've avoided half a minute of twiddling my thumbs, sends me a hit of dopamine and reinforces this behaviour.

If this were to only happen once, it wouldn't be a big deal. But when this happens multiple times a day, every day, it erodes my focus and pulls me away from what really matters, and that's something I simply can't accept.

## It's more important than ever to keep distractions in check

Companies know how these habits are formed, and they use our brain chemistry to their advantage. **Very smart people use these habit-building strategies to get us to use their products.** Maybe you've even help implement some of these strategies. 

I know I have 😐

But let's be clear: **your time and attention are not renewable resources. You have a finite amount of both.** If others are willing to go to such lengths to take them from you, you should take control and fight back.

We're in a privileged position because we know how these things work. We've lived first-hand the effort that goes into showing a badge to get users to re-open an app. 

## Get back in control by auditing yourself

**Here's the strategy I used** (inspired from Nir Eyal's new book, [Indistractable](https://www.amazon.com/Indistractable-Control-Your-Attention-Choose/dp/194883653X)) to get a grip on what was happening to me.

I started to work with a notepad by my side, where I separated the page into 4 columns:

- Time
- Distraction
- Emotion
- Ideas

For a whole week, I would try to **work through the day while taking note of the different distractions** that would pull me off task. 

For example, at 8:15, I started checking Twitter. Why? Because I felt challenged by a particular problem I was trying to solve. I fill out the first three columns: "8:15", "Checking Twitter on Tweetbot (Mac)", and "Frustration".

And now, the most important part: **in that moment, I try to figure out what would have saved me from getting distracted.** In this case, it's locking down Tweetbot so I can't just open it at the first sign of trouble.

I like this exercise because it accomplishes many things at once:

- It makes you realize when a distraction is occuring
- It cuts the habit loop short
- It slowly creates an action plan you can follow to get back in control

It's remarkable that something so simple can be so effective.

## So here's my challenge to you

**Try working one whole day with a notepad like this by your side.** When you find yourself straying off task, take a moment to figure out why. Not only will you be surprised by the results, but you'll also end up with a great little list of tasks you can do to fix these problems.

**Implement these solutions, and do the exercise again.** You'll probably find that some solutions were ineffective, or the barriers built by your past self weren't quite good enough.

That's fine, it's part of the process, stick with it.

Repeat the process and capture the tasks to fix these new issues. Slowly but surely, **this feedback loop will help you regain your focus.**

## Some Strategies for Common Problems

I've been spending the better part of this year trying to reign in the distractions around me. Here's what's worked for me, and might work for you too :)

### You're being pulled in too many directions by things that need to be done "right now"

This is a common one that can make you feel overwhelmed. What works best for this is to **take a few minutes to write everything down**, clear your psychic RAM, and focus on the tasks one by one.

### Notifications pull you onto social media

This is an easy one to fix: **put your phone (and Mac) in Do Not Disturb.** In iOS, it means only certain people will be able to contact you, and push notifications won't get through. That's step 1.

Step 2 is about auditing which apps you allow into your Notification Center. With iOS 12 and beyond, there are plenty of ways to curb notifications.

### You're constantly checking your phone

First of all, **put your phone away in a drawer or in your bag.** Some studies have shown that simply having it in your peripheral vision can be distracting.

But as iOS developers, it's probably difficult for us to work without having a phone around. And while this is true, it doesn't necessarily need to be *your* phone.

If you need a device on which to physically test, maybe you could use an old iPhone you have laying around, or use a test device provided by your employer. I mean, the iPhone itself isn't the problem, right? It's *our* iPhone that's the problem.

### You suddenly find yourself browsing the web when faced with a challenging, boring or annoying problem

This happens to me all the time, even now.

**I recommend using an app like [Cold Turkey Blocker](https://getcoldturkey.com){:target="_blank"}.** Not only can it block websites, but it can also block other apps. This was essential to keep me off of things like Tweetbot and email when I'm trying to focus on work.

## You Can Defeat Distraction

I'm happy to say that I've been able to reel back my usage of Twitter, Reddit and my phone over the past few months. This has been huge for helping me stay present in the moment, focused on my tasks, and move forward on projects that are important to me.

But I'll be the first to admit that this process is tough. **It's tough to hold ourselves back from something we have done instinctively hundreds of times. But you know what? You can do it.**

**Because if you're not going to tend to your focus and attention, who will?**

<script async data-uid="bc2e35c7e3" src="https://coach-frank-consulting.ck.page/bc2e35c7e3/index.js"></script>
