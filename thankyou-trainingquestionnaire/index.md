---
layout: page
title: Thank you for submitting your answers!
permalink: /thankyou-trainingquestionnaire/
active: training
---

I know, filling out forms is a pain. However, this will genuinely help me better serve you and your team.

### The next step

Soon you'll receive an email from me with a link to book a call. Before the call, I'll review your answers and see how we can best work together!

I eagerly await our meeting!

-Frank
