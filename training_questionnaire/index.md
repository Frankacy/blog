---
layout: page
title: Let's work together!
permalink: /training_questionnaire/
active: training
---

<div class="c-page__form">
<form id="projectform" method="POST">

<div class="form-group">
<label for="what">What prompted you to seek out training?<span class="c-page__form-mandatory"></span></label>
<textarea name="Why training?" id="what" placeholder="Tell be about what triggered the need for training in your business. Is it an immediate issue that needs solving, or is it an investment in the future health of your team?"></textarea>
</div>

<div class="form-group">
<label for="business">What are you hoping to achieve through training?</label>
<textarea name="Goals?" id="business" placeholder="Please be specific. This will allow me to better hone in on the needs of your team."></textarea>
</div>

<div class="form-group">
<label for="business">Have you ever worked with a software trainer before? If so, how did it go?</label>
<textarea name="Past experience?" id="business" placeholder="I'm curious about your past experiences with software trainers. Is this a process that you're used to, or is it your first time?"></textarea>
</div>

<div class="form-group">
<label for="business">When would you like the training to take place?</label>
<textarea name="Timeline?" id="business" placeholder="Briefly explain your expectations for the timeline."></textarea>
</div>

<div class="form-group">
<label for="business">Anything else I should know?</label>
<textarea name="Anything else?" id="business" placeholder="Share as many additional details as you'd like."></textarea>
</div>

<!--
<label for="">Who are you?<span class="c-page__form-mandatory"></span></label>
<div class="grid">
  <div class="col-1-3">
	<input type="hidden" name="Email"  placeholder="Your email">
  </div>
  <div class="col-1-3">
	<input type="hidden" name="Email"  placeholder="Your email">
  </div>
</div>

<div class="grid">
  <div class="col-2-3">
    <input name="URL" type="text" placeholder="Reference URL (optional)">
  </div>
</div>
</div>
-->

<!--
<div class="form-group">
<a href="" class="c-btn">Submit Project Details</a>
</div>
-->

<div class="form-group">
<input type="submit" value="Submit" class="c-btn">
</div>

<input id="email_input" type="hidden" name="Email"  placeholder="Your email">
<input type="hidden" name="_subject" value="New Training Lead" />
<input type="hidden" name="_next" value="http://ioscoachfrank.com/thankyou-trainingquestionnaire/index.html" />
<input type="text" name="_gotcha" style="display:none" />

</form>
</div>
<script>
    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    var contactform =  document.getElementById('projectform');
    contactform.setAttribute('action', 'https://formspree.io/' + 'trainingquestionnaire' + '@' + 'robot' + '.' + 'zapier' + '.' + 'com');

    var emailinput = document.getElementById('email_input');
    var emailparam = getUrlParameter('email');
    emailinput.setAttribute('value', emailparam);
</script>

